# JSE TASK MANAGER

## About Application
Task Manager Application

## About Developer
* **Full Name**: Ruslan Abzalov
* **E-Mail**: ruslanonetwo@gmail.com

## Tasks Results
* **JSE-00**: [jse-00](https://drive.google.com/drive/folders/1vwK52B_H550TXXs2pLqFJfb83yswBuj4?usp=sharing)
* **JSE-01**: [jse-01-task-manager-result.png](https://drive.google.com/file/d/1w3qYGjnhBTM0jpmxhF_G_OPoruwfJD99/view?usp=sharing)
* **JSE-02**: [jse-02-task-manager-result.png](https://drive.google.com/file/d/1h1hVvMiSoqK0iVvXO9F1ESBWBAJN5K8H/view?usp=sharing)