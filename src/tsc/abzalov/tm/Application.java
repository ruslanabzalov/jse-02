package tsc.abzalov.tm;

import static tsc.abzalov.tm.constants.ApplicationCommands.*;

/**
 * Основной класс приложения.
 * @author Ruslan Abzalov.
 */
public class Application {

    /**
     * Основной метод приложения.
     * @param args Аргументы командной строки.
     */
    public static void main(String... args) {
        displayWelcomeMessage();
        parseArgs(args);
    }

    /**
     * Метод, отображающий приветственное сообщение.
     * @author Ruslan Abzalov.
     */
    private static void displayWelcomeMessage() {
        System.out.println("****** WELCOME TO TASK MANAGER APPLICATION ******");
    }

    /**
     * Метод, обрабатывающий входные параметры приложения.
     * @param args Аргументы командной строки.
     * @author Ruslan Abzalov.
     */
    private static void parseArgs(String... args) {
        if (args == null || args.length == 0) {
            System.out.println("Application arguments are empty!\n" +
                    "Please, re-run the application with \"help\" command to see available commands.");
            return;
        }

        String param = args[0];
        if (CMD_HELP.equals(param)) showHelp();
        else if (CMD_ABOUT.equals(param)) showAbout();
        else if (CMD_VERSION.equals(param)) showVersion();
        else System.out.println("Command \"" + param + "\" is not available!\n" +
                                "Please, re-run the application with available input command.");
    }

    /**
     * Метод, отображающий доступные команды приложения.
     * @author Ruslan Abzalov.
     */
    private static void showHelp() {
        System.out.println("Available Commands:\n" +
                           "help: Displays all available commands.\n" +
                           "version: Displays application version.\n" +
                           "about: Displays developer info.");
    }

    /**
     * Метод, отображающий основную информацию о разработчике.
     * @author Ruslan Abzalov.
     */
    private static void showAbout() {
        System.out.println("Developer Full Name: Ruslan Abzalov\n" +
                           "Developer Email: rabzalov@tsconsulting.com");
    }

    /**
     * Метод, отображающий версию приложения.
     * @author Ruslan Abzalov.
     */
    private static void showVersion() {
        System.out.println("Version: 1.0.0");
    }

}
