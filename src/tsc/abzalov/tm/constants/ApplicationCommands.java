package tsc.abzalov.tm.constants;

/**
 * Класс, содержащий константы - команды приложения.
 */
public class ApplicationCommands {

    public static final String CMD_HELP = "help";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_VERSION = "version";

}